export default () => ({
  setver: {
    port: parseInt(process.env.SERVER_PORT, 10) || 3000,
    url: process.env.SERVER_URL,
  },
  db: {
    type: process.env.DB_TYPE || 'postgres',
    port: parseInt(process.env.DB_PORT, 10) || 5432,
    host: process.env.DB_HOST || 'localhost',
    username: process.env.DB_USERNAME || 'postgres',
    password: process.env.DB_PASSWORD || 'postgres',
    database: process.env.DB_DATABASE || 'postgres',
    synchronize: process.env.DB_SYNCHRONIZE || true,
    entities: [__dirname + '/../**/*.entity.{js,ts}']
  },
  jwt: {
    secretKey: process.env.JWT_SECRET_KEY,
    expiresIn: process.env.JWT_EXPIRES_IN || 3600,
  }
});