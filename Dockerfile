FROM node:16-alpine

WORKDIR "/app"
COPY . .
RUN yarn install --frozen-lockfile
#RUN yarn build
#RUN npm prune --production


#FROM node:16-alpine AS production
#WORKDIR "/app"
#COPY --from=builder /app/package.json ./package.json
#COPY --from=builder /app/yarn.lock ./yarn.lock
#COPY --from=builder /app/dist ./dist
#COPY --from=builder /app/node_modules ./node_modules
EXPOSE 3000

CMD [ "sh", "-c", "yarn start:dev"]

