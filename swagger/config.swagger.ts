import { DocumentBuilder } from "@nestjs/swagger";

export const configSwagger = new DocumentBuilder()
  .setTitle('Task Management')
  .setDescription('The Task Management API description')
  .setVersion('1.0')
  .addBearerAuth()
  .build();