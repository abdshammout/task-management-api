import { ArgumentMetadata, Injectable, PipeTransform } from "@nestjs/common";
import { ListsService } from "../lists.service";
import { List } from "../list.entity";

@Injectable()
export class ListIdBindPipe implements PipeTransform{
  constructor(
    private listsService: ListsService
  ) { }

  async transform(value: any, metadata: ArgumentMetadata): Promise<List> {
    return this.listsService.findById(value);
  }

}