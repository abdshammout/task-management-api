import { IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateListDto{

  @ApiProperty({
    minimum:3,
    maximum:28,
    required:true,
    example: 'task 1'
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(28)
  title: string;

  @ApiProperty({
    minimum:3,
    maximum:190,
    required:false,
    example: 'description task 1'
  })
  @IsString()
  @IsOptional()
  @MinLength(3)
  @MaxLength(190)
  description: string

}