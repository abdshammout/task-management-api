import { IsOptional, IsString, MaxLength, MinLength } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class IndexListDto{

  @ApiProperty({
    minimum:3,
    maximum:190,
    required:false,
    example: 'task'
  })
  @IsString()
  @IsOptional()
  @MinLength(3)
  @MaxLength(190)
  keySearch: string;


}