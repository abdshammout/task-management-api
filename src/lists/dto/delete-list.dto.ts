import { Transform, Type } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class DeleteListDto{

  @ApiProperty({
    required:true,
    example: '[true, \'enabled\', \'true\', 1, \'1\'] it\'s true'
  })
  @Transform(({ value }) => {
    return [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1;
  })
  @IsNotEmpty()
  softDelete: boolean

}