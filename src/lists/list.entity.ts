import {
  BaseEntity,
  Column,
  CreateDateColumn, DeleteDateColumn,
  Entity, JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { User } from "../users/user.entity";
import { Task } from "../tasks/task.entity";
import { ApiProperty } from "@nestjs/swagger";

@Entity()
export class List extends BaseEntity{

  @ApiProperty({ example: '21', description: 'The id of the List' })
  @PrimaryGeneratedColumn()
  id: number

  @ApiProperty({ example: 'list 1', description: 'The title of the List' })
  @Column({
    type: "varchar",
  })
  title: string


  @ApiProperty({ example: 'description list 1', description: 'The description of the List' })
  @Column({
    nullable: true,
    type: "text",
  })
  description : string


  @ApiProperty({ example: '33', description: 'The user id of owner the List' })
  @Column()
  userId : number


  @ApiProperty({ type: () => User })
  @ManyToOne((_type) => User, (user) => user.lists, {
    eager: false,
    lazy: false
  })
  @JoinColumn([
    { name: 'userId', referencedColumnName: 'id' },
  ])
  user: User;


  @ApiProperty({ type: () => [Task] })
  @OneToMany((_type) => Task, (task) => task.list, {
    eager: false,
    lazy: false,
    cascade: true
  })
  tasks: Task[];

  @ApiProperty({ example: '2022-06-28 14:38', description: 'The date of create the List' })
  @CreateDateColumn()
  createdAt: Date;

  @ApiProperty({ example: '2022-06-28 14:38', description: 'The date of update the List' })
  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn({
    select: false
  })
  deletedAt?: Date;
}