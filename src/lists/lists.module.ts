import { Module } from '@nestjs/common';
import { ListsService } from './lists.service';
import { ListsController } from './lists.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { List } from "./list.entity";
import { ListIdBindPipe } from "./pipes/list-id-bind.pipe";
import { CaslModule } from "../casl/casl.module";

@Module({
  imports: [
    CaslModule,
    TypeOrmModule.forFeature([List])
  ],
  providers: [ListsService, ListIdBindPipe],
  controllers: [ListsController],
  exports: [ListsService]
})
export class ListsModule {}
