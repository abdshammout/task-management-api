import { Injectable } from "@nestjs/common";
import { Repository } from "typeorm";
import { List } from "./list.entity";
import { User } from "../users/user.entity";
import { CreateListDto } from "./dto/create-list.dto";
import { IndexListDto } from "./dto/index-list.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { UpdateListDto } from "./dto/update-list.dto";
import { DeleteListDto } from "./dto/delete-list.dto";

@Injectable()
export class ListsService {
  constructor(
    @InjectRepository(List)
    private listsRepository: Repository<List>
  ) { }

  async findById(id: number): Promise<List> {
    return  this.listsRepository.findOneOrFail({
      where:{ id },
    })
  }

  async create(user: User, createLitDto: CreateListDto){
    return await this.listsRepository.create({
      title: createLitDto.title,
      description: createLitDto.description,
      userId: user.id
    }).save();
  }


  async indexMeList(user: User, indexListDto: IndexListDto) {
    const { keySearch } = indexListDto;

    const query = await this.listsRepository.createQueryBuilder('list')
      .where({ userId: user.id })
      .loadRelationCountAndMap('list.tasksNotCompletCount', 'list.tasks', 'tasksQ', qb =>
        qb.andWhere('tasksQ.isCompleted =:status', { status: false })
      )
      .loadRelationCountAndMap('list.tasksCount', 'list.tasks')

    if (keySearch){
      query.andWhere(
        '(LOWER(list.title) LIKE LOWER(:keySearch) OR LOWER(list.description) LIKE LOWER(:keySearch))',
        { keySearch: `%${keySearch}%` },
      );
    }

    return query.getMany();
  }


  async show(list: List): Promise<List> {
    return this.listsRepository.createQueryBuilder('list')
      .where({ id: list.id })
      .loadRelationCountAndMap('list.tasksNotCompletCount', 'list.tasks', 'tasksQ', qb =>
        qb.andWhere('tasksQ.isCompleted =:status', { status: false })
      )
      .loadRelationCountAndMap('list.tasksCount', 'list.tasks')
      .getOne()
  }

  async update(list: List, updateListDto: UpdateListDto): Promise<List> {
    list.title = updateListDto.title
    list.description = updateListDto.description
    return await this.listsRepository.save(list)
  }


  async delete(list: List, deleteListDto: DeleteListDto): Promise<void> {
    const { softDelete } = deleteListDto;

    if (softDelete)
      await this.listsRepository.softDelete(list.id)
    else
      await this.listsRepository.delete(list.id)
  }

}
