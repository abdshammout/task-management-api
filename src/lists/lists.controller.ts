import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get, HttpCode, HttpStatus,
  Param,
  Patch,
  Post,
  Query
} from "@nestjs/common";
import { ListsService } from "./lists.service";
import { GetUser } from "../auth/get-user.decorator";
import { User } from "../users/user.entity";
import { CreateListDto } from "./dto/create-list.dto";
import { IndexListDto } from "./dto/index-list.dto";
import { List } from "./list.entity";
import { ListIdBindPipe } from "./pipes/list-id-bind.pipe";
import { UpdateListDto } from "./dto/update-list.dto";
import { DeleteListDto } from "./dto/delete-list.dto";
import { CaslAbilityFactory } from "../casl/casl-ability.factory";
import { Action } from "../helper/Action";
import { ApiBearerAuth, ApiBody, ApiParam, ApiResponse, ApiTags } from "@nestjs/swagger";

@ApiBearerAuth()
@ApiTags('lists')
@Controller()
export class ListsController {
  constructor(
    private listsService: ListsService,
    private caslAbilityFactory: CaslAbilityFactory
  ) {}


  @Post()
  @ApiBody({ type: CreateListDto })
  @ApiResponse({
    status: 200,
    description: 'create list',
    type: List,
  })
  async create(
    @GetUser() user: User,
    @Body() createListDto: CreateListDto
  ): Promise<List> {
      return await this.listsService.create(user, createListDto);
  }


  @Get()
  @ApiResponse({
    status: 200,
    description: 'index of list',
    type: [List],
  })
  async index(
    @GetUser() user: User,
    @Query() indexListDto: IndexListDto
  ): Promise<List[]> {
    return await this.listsService.indexMeList(user, indexListDto);
  }


  @Get('/:list')
  @ApiParam({
    name: 'list',
    required: true,
    description: 'id of list to show',
    schema: { oneOf: [{type: 'string'}, {type: 'integer'}]},
    type: 'integer',
  })
  @ApiResponse({
    status: 200,
    description: 'show list',
    type: List,
  })
  async show(
    @GetUser() user: User,
    @Param('list', ListIdBindPipe) list: List,
  ): Promise<List> {
    const ability = this.caslAbilityFactory.createForUser(user);
    if (ability.can(Action.Read, list)) {
      return this.listsService.show(list);
    }
    throw new ForbiddenException()
  }


  @Patch('/:list')
  @ApiParam({
    name: 'list',
    required: true,
    description: 'id of list to show',
    schema: { oneOf: [{type: 'string'}, {type: 'integer'}]},
    type: 'integer',
  })
  @ApiBody({type: UpdateListDto})
  @ApiResponse({
    status: 200,
    description: 'update list',
    type: List,
  })
  async update(
    @GetUser() user: User,
    @Param('list', ListIdBindPipe) list: List,
    @Body() updateListDto: UpdateListDto
  ): Promise<List> {
    const ability = this.caslAbilityFactory.createForUser(user);
    if (ability.can(Action.Update, list)) {
      return await this.listsService.update(list, updateListDto);
    }
    throw new ForbiddenException()
  }


  @Delete('/:list')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiParam({
    name: 'list',
    required: true,
    description: 'id of list to show',
    schema: { oneOf: [{type: 'string'}, {type: 'integer'}]},
    type: 'integer',
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'delete list',
    type: null,
  })
  async delete(
    @GetUser() user: User,
    @Param('list', ListIdBindPipe) list: List,
    @Query() deleteListDto: DeleteListDto
  ): Promise<void> {
    const ability = this.caslAbilityFactory.createForUser(user);
    if (ability.can(Action.Delete, list)) {
      return await this.listsService.delete(list, deleteListDto);
    }
    throw new ForbiddenException()
  }


}
