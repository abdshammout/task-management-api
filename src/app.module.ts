import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from "@nestjs/typeorm";
import { ListsModule } from './lists/lists.module';
import { TasksModule } from './tasks/tasks.module';
import { RouterModule } from "@nestjs/core";
import { CaslModule } from './casl/casl.module';
import { ConfigModule, ConfigService } from "@nestjs/config";
import configuration from "../config/configuration";


const routes = [
  {
    path: 'auth',
    module: AuthModule,
  },
  {
    path: 'users',
    module: UsersModule,
    children: [
      {
        path: '/me-account/lists',
        module: ListsModule,
        children: [
          {
            path: '/:list/tasks',
            module: TasksModule,
          }
        ]
      }
    ]
  },
];


@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      expandVariables: true,
      load: [configuration],
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => config.get('db'),
      inject: [ConfigService],
    }),
    UsersModule,
    AuthModule,
    ListsModule,
    TasksModule,
    RouterModule.register(routes),
    CaslModule,
  ],
  controllers: [],
})
export class AppModule {}
