import { BadRequestException } from "@nestjs/common";
import { diskStorage } from "multer";


const imageFileFilter = (req, file, callback) => {
  if (!file){
    return callback(new BadRequestException('Onassdadas!'), false);
  }
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return callback(new BadRequestException('Only image files are allowed!'), false);
  }
  callback(null, true);
};

const editImageName = (req, file, callback) => {
  const name = 'avatar';
  const currentDate = Date.now().toString();
  const fileExtName = '.'+file.originalname.split('.')[1];
  callback(null, `${name}-${currentDate}${fileExtName}`);
};

export const imageOptions = {
  fileFilter: imageFileFilter,
    storage: diskStorage({
  destination: './avatars',
  filename: editImageName,
}),

}