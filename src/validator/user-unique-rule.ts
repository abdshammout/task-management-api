import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface
} from "class-validator";
import { Injectable } from "@nestjs/common";
import { UsersService } from "../users/users.service";

@ValidatorConstraint({ name: 'UserUnique', async: true })
@Injectable()
export class UserUniqueRule implements ValidatorConstraintInterface {
  constructor(
    private readonly usersService: UsersService
  ) {}

  async validate(value: string) {
      const user = await this.usersService.findByUsername(value);
      return !user
  }

  defaultMessage(args: ValidationArguments) {
    return `username already exist`;
  }

}

export function UserUnique(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'UserUnique',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: UserUniqueRule,
    });
  };
}
