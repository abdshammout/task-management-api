import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { SignupDto } from "../auth/dto/signup.dto";
import { UpdateUserInfoDto } from "./dto/update-user-info.dto";
import { User } from "./user.entity";

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>
  ) {}


  async findById(id: number): Promise<User> {
    return this.usersRepository.findOneOrFail({
      where:{ id }
    })
  }


  async createUser(signupDto: SignupDto){
    const user = await this.usersRepository.create({
      username: signupDto.username,
      name: signupDto.name,
      photo: signupDto.photoUrl,
      password: signupDto.password,
    }).save()
    delete user.password;
    return user
  }


  async show(user: User){
    return user
  }


  async updateUserInfo(user: User, updateUserInfoDto: UpdateUserInfoDto){
    user.name = updateUserInfoDto.name;
    return await this.usersRepository.save(user);
  }


  async updateUserPhoto(user: User, photoUrl){
    user.photo = photoUrl;
    return await this.usersRepository.save(user);
  }


  async findByUsername(username: string): Promise<User>{
    return this.usersRepository.findOne
    ({
      where: {username},
      select: ['id', 'name', 'username', 'password', "photo", 'createdAt', 'updatedAt']
    });
  }


}
