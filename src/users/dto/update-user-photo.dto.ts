import { ApiProperty } from "@nestjs/swagger";

export class UpdateUserPhotoDto{

  @ApiProperty({ type: 'string', format: 'binary' })
  photo: any;

}