import { IsOptional, IsString, MaxLength, MinLength } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateUserInfoDto {

  @ApiProperty({
    minimum:2,
    maximum:190,
    required:false,
  })
  @IsString()
  @IsOptional()
  @MinLength(2)
  @MaxLength(190)
  name: string

}