import {
  Body,
  Controller,
  Get, HttpCode, HttpStatus,
  Param,
  Patch,
  Query,
  Res,
  UploadedFile,
  UseInterceptors
} from "@nestjs/common";
import { UsersService } from "./users.service";
import { User } from "./user.entity";
import { UpdateUserInfoDto } from "./dto/update-user-info.dto";
import { FileInterceptor } from "@nestjs/platform-express";
import { imageOptions } from "../helper/config-upload-file";
import { GetUser } from "../auth/get-user.decorator";
import { PublicRoute } from "../auth/guard/public-route.decorator";
import { TasksService } from "../tasks/tasks.service";
import { Task } from "../tasks/task.entity";
import { IndexTaskDto } from "../tasks/dto/index-task.dto";
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiQuery, ApiResponse, ApiTags } from "@nestjs/swagger";
import { UpdateUserPhotoDto } from "./dto/update-user-photo.dto";
import { ConfigService } from "@nestjs/config";

@ApiBearerAuth()
@ApiTags('users')
@Controller()
export class UsersController {
  constructor(
    private configService: ConfigService,
    private tasksService: TasksService,
    private usersService: UsersService
  ) {}


  @Get('/me-account/info')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'get info current account',
    type: User,
  })
  async getMeInfo(
    @GetUser() user: User,
  ): Promise<User> {
    return await this.usersService.show(user);
  }

  @Patch('/me-account/update-info')
  @HttpCode(HttpStatus.OK)
  @ApiBody({ type: UpdateUserInfoDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'update info current account',
    type: User,
  })
  async updateUserInfo(
    @GetUser() user: User,
    @Body() updateUserInfoDto: UpdateUserInfoDto
  ): Promise<User> {
    return await this.usersService.updateUserInfo(user, updateUserInfoDto)
  }


  @Patch('/me-account/update-photo')
  @HttpCode(HttpStatus.OK)
  @ApiBody({ type: UpdateUserPhotoDto })
  @ApiConsumes('multipart/form-data')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'update photo current account',
    type: User,
  })
  @UseInterceptors(FileInterceptor('photo',imageOptions))
  async updateUserPhoto(
    @GetUser() user: User,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<User> {
    const photoUrl = `${this.configService.get('server.url')}users/avatars/${file.filename}`
    return await this.usersService.updateUserPhoto(user, photoUrl)
  }


  @Get('/me-account/tasks')
  @HttpCode(HttpStatus.OK)
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'get tasks current account',
    type: [Task],
  })
  async getMeTasks(
    @GetUser() user: User,
    @Query() indexTaskDto: IndexTaskDto
  ): Promise<Task[]> {
    return await this.tasksService.index(user, indexTaskDto);
  }



  @Get('avatars/:fileId')
  @PublicRoute()
  async serveAvatar(
    @Param('fileId') fileId,
    @Res() res
  ): Promise<any> {
    res.sendFile(fileId, { root: 'avatars'});
  }

}
