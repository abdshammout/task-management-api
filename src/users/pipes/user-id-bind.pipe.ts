import { ArgumentMetadata, Injectable, PipeTransform } from "@nestjs/common";
import { UsersService } from "../users.service";

@Injectable()
export class UserIdBindPipe implements PipeTransform{
  constructor(
    private usersService: UsersService
  ) { }

  transform(value: any, metadata: ArgumentMetadata): any {
    return this.usersService.findById(value);
  }

}