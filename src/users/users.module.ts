import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "./user.entity";
import { UserUniqueRule } from "../validator/user-unique-rule";
import { TasksModule } from "../tasks/tasks.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    TasksModule
  ],
  providers: [UsersService, UserUniqueRule],
  controllers: [UsersController]
})
export class UsersModule {}
