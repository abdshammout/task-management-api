import {
  BaseEntity, BeforeInsert,
  Column,
  CreateDateColumn,
  Entity, OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { List } from "../lists/list.entity";
import { ApiProperty } from "@nestjs/swagger";
const bcrypt = require('bcryptjs')

@Entity()
export class User extends BaseEntity{

  @ApiProperty({ example: '33', description: 'The id of the User' })
  @PrimaryGeneratedColumn()
  id: number

  @ApiProperty({ example: 'abdshammout', description: 'The username of the User' })
  @Column({
    unique: true,
    type: "varchar",
  })
  username: string

  @ApiProperty({ example: 'abd alrahman shammout', description: 'The name of the User' })
  @Column({
    nullable: true,
    type: "varchar",
  })
  name: string


  @ApiProperty({ example: 'https://domin.com/users/avatars/avatar-1656930800288.jpg', description: 'The photo of the User' })
  @Column({
    nullable: true,
    type: "varchar",
    default: 'https://listimg.pinclipart.com/picdir/s/148-1486972_mystery-man-avatar-circle-clipart.png'
  })
  photo: string

  // @Exclude()
  @ApiProperty({ example: 'Abd!123@', description: 'The password of the User' })
  @Column({
    select: false,
    type: "varchar",
  })
  password: string


  @ApiProperty({ type: () => [List] })
  @OneToMany((_type) => List, (list) => list.user, {
    eager: false ,
    lazy: false ,
    cascade: true
  })
  lists: List[];

  @ApiProperty({ example: '2022-06-28 14:38', description: 'The date of create the User' })
  @CreateDateColumn()
  createdAt: Date;

  @ApiProperty({ example: '2022-06-28 14:38', description: 'The date of update the User' })
  @UpdateDateColumn()
  updatedAt: Date;


  @BeforeInsert()
  async hashPassword() {
    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt);
  }

}