import { NestFactory, Reflector } from "@nestjs/core";
import { AppModule } from './app.module';
import { ValidationPipe } from "@nestjs/common";
import { AuthGuard } from "./auth/guard/auth.gaurd";
import { useContainer } from "class-validator";
import { HttpExceptionFilter } from "./filters/http-exception.filter";
import { SwaggerModule } from "@nestjs/swagger";
import { ConfigService } from "@nestjs/config";
import { configSwagger } from "../swagger/config.swagger";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  const reflector = app.get(Reflector);
  app.useGlobalGuards(new AuthGuard(reflector));
  app.useGlobalPipes(new ValidationPipe({ transform: true} ));
  app.useGlobalFilters(new HttpExceptionFilter());

  const document = SwaggerModule.createDocument(app, configSwagger);
  SwaggerModule.setup('api', app, document,);

  const configService = app.get(ConfigService);
  await app.listen(configService.get<number>('setver.port'));
}

bootstrap();
