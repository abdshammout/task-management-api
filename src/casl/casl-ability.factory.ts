import { Injectable } from "@nestjs/common";
import { Ability, AbilityBuilder, AbilityClass, ExtractSubjectType, InferSubjects, PureAbility } from "@casl/ability";
import { User } from "../users/user.entity";
import { Action } from "../helper/Action";
import { List } from "../lists/list.entity";
import { Task } from "../tasks/task.entity";

type Subjects = InferSubjects<typeof Task | typeof List | typeof User> | 'all';

export type AppAbility = PureAbility<[Action, Subjects]>;

export const AppAbility = Ability as AbilityClass<AppAbility>;

@Injectable()
export class CaslAbilityFactory {
  createForUser(_user: User) {
    const { can, cannot, build } = new AbilityBuilder(AppAbility);

    //--- list policy ---
    can(Action.Read, List, { 'userId': _user.id });
    can(Action.Update, List, { 'userId': _user.id });
    can(Action.Delete, List, { 'userId': _user.id });

    //--- task policy ---
    can(Action.Read, Task, { ['userId']: _user.id });
    can(Action.Update, Task, { 'userId': _user.id });
    can(Action.Delete, Task, { 'userId': _user.id });


    return build({
      detectSubjectType: item =>
        item.constructor as ExtractSubjectType<Subjects>,
    });
  }
}