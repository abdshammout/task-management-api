import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  UnauthorizedException
} from "@nestjs/common";
import { Request, Response } from 'express';
import { CannotCreateEntityIdMapError, EntityNotFoundError, QueryFailedError, TypeORMError } from "typeorm";

@Catch( TypeORMError, HttpException )
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    let status
    let message = (exception as any).message;
    let code = 'HttpException';
    let messageValidation;
    console.log(exception.constructor)
    if (exception instanceof HttpException){
      status = exception.getStatus();
      message = exception.message;
      code =  exception.name;
      messageValidation =  exception.getResponse()['message'];
    } else if (exception instanceof TypeORMError){
      switch (exception.constructor) {
        case QueryFailedError || CannotCreateEntityIdMapError:
          status = HttpStatus.UNPROCESSABLE_ENTITY
          break;
        case EntityNotFoundError:
          status = HttpStatus.NOT_FOUND
          break;
        default:
          status = HttpStatus.BAD_REQUEST
      }
      message = exception.message;
      code = exception.name;
    }

    response
      .status(status)
      .json({
        message,
        statusCode: status,
        code,
        timestamp: new Date().toISOString(),
        path: request.url,
        messageValidation,
      });
  }
}