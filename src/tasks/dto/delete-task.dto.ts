import { Transform } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

export class DeleteTaskDto{

  @ApiProperty({
    required:true,
    example: '[true, \'enabled\', \'true\', 1, \'1\'] it\'s true'
  })
  @Transform(({ value }) => {
    return [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1;
  })
  softDelete: boolean

}