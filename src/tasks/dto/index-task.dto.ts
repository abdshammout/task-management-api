import {
  IsDateString,
  IsEnum,
  IsOptional,
  IsString,
  MaxLength,
  MinLength
} from "class-validator";
import { TaskPriority } from "../task-priority.enum";
import { Transform } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

export class IndexTaskDto{

  @ApiProperty({
    minimum:3,
    maximum:200,
    required:false,
    example: 'hello'
  })
  @IsString()
  @IsOptional()
  @MinLength(3)
  @MaxLength(200)
  keySearch: string


  @ApiProperty({
    required:false,
    example: '[true, \'enabled\', \'true\', 1, \'1\'] it\'s true'
  })
  @Transform(({ value }) => {
    return [true, 'enabled', 'true', 1, '1'].indexOf(value) > -1;
  })
  @IsOptional()
  isCompleted: boolean


  @ApiProperty({
    required:false,
    example: '2022-08-23'
  })
  @IsDateString()
  @IsOptional()
  fromDateTime: Date

  @ApiProperty({
    required:false,
    example: '2022-08-28'
  })
  @IsDateString()
  @IsOptional()
  toDateTime: Date

  @ApiProperty({
    enum: TaskPriority,
    required:false,
    example: 'P1'
  })
  @IsOptional()
  @IsEnum(TaskPriority, {
    message: "priority must be a valid enum value (P1 - P2 - P3)"
  })
  priority: TaskPriority


}