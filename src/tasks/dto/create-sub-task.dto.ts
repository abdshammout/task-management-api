import {
  IsDateString,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength
} from "class-validator";
import { TaskPriority } from "../task-priority.enum";
import { ApiProperty } from "@nestjs/swagger";

export class CreateSubTaskDto{

  @ApiProperty({
    minimum:3,
    maximum:28,
    required:true,
    example: 'hello'
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(28)
  title: string


  @ApiProperty({
    minimum:3,
    maximum:200,
    required:false,
    example: 'hello I AM NOTE'
  })
  @IsString()
  @IsOptional()
  @MinLength(3)
  @MaxLength(200)
  note: string


  @ApiProperty({
    required:false,
    example: '2022-08-23'
  })
  @IsDateString()
  @IsOptional()
  DueDateTime: Date


  @ApiProperty({
    minimum:3,
    maximum:190,
    required:false,
    example: 'hello I AM location'
  })
  @IsString()
  @IsOptional()
  @MinLength(3)
  @MaxLength(190)
  location: string


  @ApiProperty({
    enum: TaskPriority,
    required:false,
    example: 'P1'
  })
  @IsOptional()
  @IsEnum(TaskPriority)
  priority: TaskPriority


}