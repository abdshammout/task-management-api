import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query
} from "@nestjs/common";
import { Task } from "./task.entity";
import { GetUser } from "../auth/get-user.decorator";
import { User } from "../users/user.entity";
import { CreateTaskDto } from "./dto/create-task.dto";
import { TasksService } from "./tasks.service";
import { ListIdBindPipe } from "../lists/pipes/list-id-bind.pipe";
import { List } from "../lists/list.entity";
import { IndexTaskDto } from "./dto/index-task.dto";
import { TaskIdBindPipe } from "./pipes/task-id-bind.pipe";
import { UpdateTaskDto } from "./dto/update-task.dto";
import { DeleteTaskDto } from "./dto/delete-task.dto";
import { Action } from "../helper/Action";
import { CaslAbilityFactory } from "../casl/casl-ability.factory";
import { ApiBearerAuth, ApiBody, ApiParam, ApiResponse, ApiTags } from "@nestjs/swagger";

@ApiBearerAuth()
@ApiTags('tasks')
@Controller()
export class TasksController {
  constructor(
    private tasksServices: TasksService,
  private caslAbilityFactory: CaslAbilityFactory
) { }


  @Post()
  @ApiBody({ type: CreateTaskDto })
  @ApiResponse({
    status: 200,
    description: 'create task',
    type: Task,
  })
  async create(
    @GetUser() user: User,
    @Param('list', ListIdBindPipe) list: List,
    @Body() createTaskDto: CreateTaskDto
  ): Promise<Task> {
    const ability = this.caslAbilityFactory.createForUser(user);
    if (ability.can(Action.Read, list)) {
      return await this.tasksServices.create(list, createTaskDto)
    }
    throw new ForbiddenException()
  }


  @Get()
  @ApiParam({
    name: 'list',
    required: true,
    description: 'id of list',
    schema: { oneOf: [{type: 'string'}, {type: 'integer'}]},
    type: 'integer',
  })
  @ApiResponse({
    status: 200,
    description: 'index of task by list',
    type: [Task],
  })
  async indexByList(
    @GetUser() user: User,
    @Param('list', ListIdBindPipe) list: List,
    @Query() indexTaskDto: IndexTaskDto
  ): Promise<Task[]> {
    const ability = this.caslAbilityFactory.createForUser(user);
    if (ability.can(Action.Read, list)) {
      return this.tasksServices.indexByList(list, indexTaskDto)
    }
    throw new ForbiddenException()
  }



  @Get('/:task/sub-tasks')
  @ApiParam({
    name: 'list',
    required: true,
    description: 'id of list',
    schema: { oneOf: [{type: 'string'}, {type: 'integer'}]},
    type: 'integer',
  })
  @ApiParam({
    name: 'task',
    required: true,
    description: 'id of task',
    schema: { oneOf: [{type: 'string'}, {type: 'integer'}]},
    type: 'integer',
  })
  @ApiResponse({
    status: 200,
    description: 'index of task by perant task',
    type: [Task],
  })
  async indexByParentTask(
    @GetUser() user: User,
    @Param('list', ListIdBindPipe) list: List,
    @Param('task', TaskIdBindPipe) task: Task,
    @Query() indexTaskDto: IndexTaskDto
  ): Promise<Task[]> {
    return this.tasksServices.indexByParentTask(task, indexTaskDto)
  }



  @Get('/:task')
  @ApiParam({
    name: 'list',
    required: true,
    description: 'id of list',
    schema: { oneOf: [{type: 'string'}, {type: 'integer'}]},
    type: 'integer',
  })
  @ApiParam({
    name: 'task',
    required: true,
    description: 'id of task to show',
    schema: { oneOf: [{type: 'string'}, {type: 'integer'}]},
    type: 'integer',
  })
  @ApiResponse({
    status: 200,
    description: 'show task',
    type: Task,
  })
  async show(
    @GetUser() user: User,
    @Param('list', ListIdBindPipe) list: List,
    @Param('task', TaskIdBindPipe) task: Task,
  ): Promise<Task> {
    return this.tasksServices.show(task)
  }


  @Patch('/:task')
  @ApiParam({
    name: 'list',
    required: true,
    description: 'id of list',
    schema: { oneOf: [{type: 'string'}, {type: 'integer'}]},
    type: 'integer',
  })
  @ApiParam({
    name: 'task',
    required: true,
    description: 'id of task',
    schema: { oneOf: [{type: 'string'}, {type: 'integer'}]},
    type: 'integer',
  })
  @ApiBody({type: UpdateTaskDto})
  @ApiResponse({
    status: 200,
    description: 'update task',
    type: Task,
  })
  async update(
    @GetUser() user: User,
    @Param('list', ListIdBindPipe) list: List,
    @Param('task', TaskIdBindPipe) task: Task,
    @Body() updateTaskDto: UpdateTaskDto
  ): Promise<Task> {
    return this.tasksServices.update(task, updateTaskDto)
  }


  @Delete('/:task')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiParam({
    name: 'list',
    required: true,
    description: 'id of list',
    schema: { oneOf: [{type: 'string'}, {type: 'integer'}]},
    type: 'integer',
  })
  @ApiParam({
    name: 'task',
    required: true,
    description: 'id of task',
    schema: { oneOf: [{type: 'string'}, {type: 'integer'}]},
    type: 'integer',
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'delete task',
    type: null,
  })
  async delete(
    @GetUser() user: User,
    @Param('list', ListIdBindPipe) list: List,
    @Param('task', TaskIdBindPipe) task: Task,
    @Query() deleteTaskDto: DeleteTaskDto
  ): Promise<void> {
   const  { softDelete } = deleteTaskDto;
    return this.tasksServices.delete(task, softDelete)
  }
}
