import { ArgumentMetadata, Injectable, PipeTransform } from "@nestjs/common";
import { TasksService } from "../tasks.service";

@Injectable()
export class TaskIdBindPipe implements PipeTransform{
  constructor(
    private tasksService: TasksService
  ) { }

  transform(value: any, metadata: ArgumentMetadata): any {
    return this.tasksService.findById(value);
  }

}