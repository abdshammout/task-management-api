import {
  BaseEntity,
  Column,
  CreateDateColumn, DeleteDateColumn,
  Entity, JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";
import { List } from "../lists/list.entity";
import { TaskPriority } from "./task-priority.enum";
import { ApiProperty } from "@nestjs/swagger";

@Entity()
export class Task extends BaseEntity{

  @ApiProperty({ example: '7', description: 'The id of the Task' })
  @PrimaryGeneratedColumn()
  id: number

  @ApiProperty({ example: 'task 1', description: 'The title of the Task' })
  @Column({
    type: "varchar",
  })
  title: string

  @ApiProperty({ example: 'note task 1', description: 'The note of the Task' })
  @Column({
    nullable: true,
    type: "text",
  })
  note: string

  @ApiProperty({ example: true, description: 'The status of the Task' })
  @Column({
    nullable: true,
    type: "boolean",
    default: false
  })
  isCompleted: boolean

  @ApiProperty({ example: '2022-06-28 14:38', description: 'The Due Date of the Task' })
  @Column({
    nullable: true,
    type: 'timestamp'
  })
  DueDateTime: Date


  @ApiProperty({ example: 'UAE', description: 'The location of the Task' })
  @Column({
    nullable: true,
    type: 'varchar'
  })
  location : string


  @ApiProperty({ enum:TaskPriority, example: TaskPriority.P2, description: 'The location of the Task' })
  @Column({
    nullable: true,
    type: "enum",
    enum: TaskPriority,
  })
  priority : TaskPriority;


  @ApiProperty({ example: 21, description: 'The owner list id of the Task' })
  @Column()
  listId : number


  @ApiProperty({ nullable:true, example: 7, description: 'The owner task id of the sub Task' })
  @Column({
    nullable: true,
  })
  perantTaskId : number

  @ApiProperty({ type: () => List })
  @ManyToOne((_type) => List, (list) => list.tasks,{
    eager: false,
    lazy: true,
  })
  @JoinColumn([
    { name: 'listId', referencedColumnName: 'id'},
  ])
  list: List;


  @ApiProperty({ type: () => Task, name:'perantTask' })
  @ManyToOne((_type) => Task, (perantTask) => perantTask.subTasks, {
    eager: false,
    lazy: false
  })
  @JoinColumn([
    { name: 'perantTaskId', referencedColumnName: 'id' },
  ])
  perantTask: Task;


  @ApiProperty({ type: () => [Task] , name:'subTasks'})
  @OneToMany((_type) => Task, (task) => task.perantTask, {
    eager: false,
    lazy: true,
    cascade: true
  })
  subTasks: Task[];


  @ApiProperty({ example: '2022-06-28 14:38', description: 'The date of create the Task' })
  @CreateDateColumn()
  createdAt: Date;

  @ApiProperty({ example: '2022-06-28 14:38', description: 'The date of update the Task' })
  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn({
    select: false
  })
  deletedAt?: Date;
}