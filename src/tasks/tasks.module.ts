import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { TasksController } from './tasks.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Task } from "./task.entity";
import { TaskIdBindPipe } from "./pipes/task-id-bind.pipe";
import { ListsModule } from "../lists/lists.module";
import { CaslModule } from "../casl/casl.module";

@Module({
  imports: [
    CaslModule,
    ListsModule,
    TypeOrmModule.forFeature([Task])
  ],
  providers: [TasksService, TaskIdBindPipe],
  controllers: [TasksController],
  exports: [TasksService]
})
export class TasksModule {}
