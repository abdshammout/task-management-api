import { Injectable } from '@nestjs/common';
import { List } from "../lists/list.entity";
import { IsNull, Repository } from "typeorm";
import { Task } from "./task.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { CreateTaskDto } from "./dto/create-task.dto";
import { IndexTaskDto } from "./dto/index-task.dto";
import { SelectQueryBuilder } from "typeorm/query-builder/SelectQueryBuilder";
import { User } from "../users/user.entity";
import { UpdateTaskDto } from "./dto/update-task.dto";

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(Task)
    private tasksRepository: Repository<Task>
  ) { }

  async findById(id: number): Promise<Task> {
    return this.tasksRepository.findOneOrFail({
      where:{ id },
    })
  }



  async create(list: List, createTaskDto: CreateTaskDto){
    const task = await this.tasksRepository.create({
      title: createTaskDto.title,
      note: createTaskDto.note,
      location: createTaskDto.location,
      priority: createTaskDto.priority,
      DueDateTime: createTaskDto.DueDateTime,
      listId: list.id
    }).save()

    const subTasks = []
    for (let i = 0; i < createTaskDto.subTasks.length; i++) {
      const subTask = await this.tasksRepository.create({
        title: createTaskDto.subTasks[i].title,
        note: createTaskDto.subTasks[i].note,
        location: createTaskDto.subTasks[i].location,
        priority: createTaskDto.subTasks[i].priority,
        DueDateTime: createTaskDto.subTasks[i].DueDateTime,
        perantTaskId: task.id,
        listId: list.id
      })
      subTasks.push(subTask)
    }
    await this.tasksRepository.save(subTasks)
    return task;
  }


  async index(user: User, indexTaskDto: IndexTaskDto): Promise<Task[]> {
    let query = this.tasksRepository.createQueryBuilder('task')
      .leftJoinAndSelect('task.list', 'list')
      .where('list.userId = :userId', {userId: user.id} )
      .andWhere({perantTaskId: IsNull()})

    query = this.filterTaskByQuery(indexTaskDto, query);

    return query.getMany();
  }


  async indexByList(list: List, indexTaskDto: IndexTaskDto){
    let query = this.tasksRepository.createQueryBuilder('task')
      .where('task.listId = :listId', {listId: list.id})
      .andWhere({perantTaskId: IsNull()})

    query = this.filterTaskByQuery(indexTaskDto, query);

    return query.getMany();
  }



  async indexByParentTask(task: Task, indexTaskDto: IndexTaskDto){
    return task.subTasks;
  }

  async show(task: Task){
    return task;
  }

  async update(task: Task, updateTaskDto: UpdateTaskDto){
    task.title = updateTaskDto.title
    task.note = updateTaskDto.note
    task.DueDateTime = updateTaskDto.DueDateTime
    task.priority = updateTaskDto.priority
    task.location = updateTaskDto.location
    return this.tasksRepository.save(task, { reload: true })
  }

  async delete(task: Task, softDelete): Promise<void>{
    if (softDelete)
      await this.tasksRepository.softRemove(task);
    else
      await this.tasksRepository.remove(task);
  }


  filterTaskByQuery(indexTaskDto: IndexTaskDto, query: SelectQueryBuilder<Task>){
    const { keySearch, isCompleted, priority, fromDateTime, toDateTime} = indexTaskDto;

    if (keySearch){
      query.andWhere(
        '(LOWER(task.title) LIKE LOWER(:keySearch) ' +
        'OR LOWER(task.note) LIKE LOWER(:keySearch) ' +
        'OR LOWER(task.location) LIKE LOWER(:keySearch))',
        { keySearch: `%${keySearch}%` },
      );
    }

    if (priority) {
      query.andWhere('task.priority = :priority', { priority });
    }

    if (isCompleted){
      query.andWhere('task.isCompleted = :isCompleted', { isCompleted });
    }

    if (fromDateTime){
      query.andWhere('task.DueDateTime >= :fromDateTime',
        { fromDateTime })
    }

    if (toDateTime){
      query.andWhere('task.DueDateTime <= :toDateTime',
        { toDateTime })
    }
    return query;
  }


}
