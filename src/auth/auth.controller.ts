import { Body, Controller, Get, HttpCode, HttpStatus, Post, UploadedFile, UseInterceptors } from "@nestjs/common";
import { FileInterceptor } from "@nestjs/platform-express";
import { imageOptions } from "../helper/config-upload-file";
import { AuthService } from "./auth.service";
import { SignupDto } from "./dto/signup.dto";
import { LoginDto } from "./dto/login.dto";
import { PublicRoute } from "./guard/public-route.decorator";
import { ApiBody, ApiConsumes, ApiOkResponse, ApiResponse, ApiTags } from "@nestjs/swagger";
import { AuthResponseInterface } from "./interfaces/auth.response.interface";
import { ConfigService } from "@nestjs/config";


@ApiTags('auth')
@Controller()
export class AuthController {
  constructor(
    private configService: ConfigService,
    private authService: AuthService
  ) {}


  @Post('/signup')
  @ApiConsumes('multipart/form-data')
  @HttpCode(HttpStatus.OK)
  @ApiBody({ type: SignupDto })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'create user',
    type: AuthResponseInterface,
  })
  @PublicRoute()
  @UseInterceptors(FileInterceptor('photo',imageOptions))
  async signup(
    @UploadedFile() file: Express.Multer.File,
    @Body() signupDto: SignupDto,
  ): Promise<AuthResponseInterface> {
    signupDto.photoUrl = `${this.configService.get('SERVER_URL')}users/avatars/${file.filename}`
    return this.authService.signup(signupDto);
  }


  @Post('/login')
  @HttpCode(HttpStatus.OK)
  @ApiBody({ type: LoginDto })
  @ApiOkResponse({
    status: HttpStatus.OK,
    description: 'login user',
    type: AuthResponseInterface,
  })
  @PublicRoute()
  public login(
    @Body() loginDto: LoginDto
  ): Promise<AuthResponseInterface> {
    return this.authService.login(loginDto);
  }
@Get()
@PublicRoute()
public aaaa(
) {
    console.log(this.configService.get('SERVER_URL'))
    // console.log(this.configService.get('database.host'))
  return JSON.stringify(this.configService.get('SERVER_URL'));
}

}
