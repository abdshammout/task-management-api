import { User } from "../../users/user.entity";
import { ApiProperty } from "@nestjs/swagger";

export class AuthResponseInterface {

  @ApiProperty({ example: 'eyJhbGciOiJIU**********************', description: 'The access Token of the User' })
  accessToken: string

  @ApiProperty({ type: () => User })
  user: User

}