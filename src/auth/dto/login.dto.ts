import { IsNotEmpty, IsString, Matches, MaxLength, MinLength } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class LoginDto{

  @ApiProperty({
    minimum:4,
    maximum:28,
    required:true,
    example: 'abdshammout'
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(4)
  @MaxLength(28)
  username: string

  @ApiProperty({
    minimum:8,
    maximum:32,
    required:true,
    example: 'Abd!123@'
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  @MaxLength(32)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'password is too weak',
  })
  password: string

}