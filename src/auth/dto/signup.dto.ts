import {
  IsNotEmpty,
  IsOptional,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from "class-validator";
import { UserUnique } from "../../validator/user-unique-rule";
import { ApiProperty } from "@nestjs/swagger";

export class SignupDto{

  @ApiProperty({
    minimum:4,
    maximum:28,
    required:true,
    example: 'abdshammout'
  })
  @IsString()
  @IsNotEmpty()
  @UserUnique()
  @MinLength(4)
  @MaxLength(28)
  username: string

  @ApiProperty({
    minimum:8,
    maximum:32,
    required:true,
    example: 'Abd!123@'
  })
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  @MaxLength(32)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'password is too weak',
  })
  password: string


  @ApiProperty({
    minimum:2,
    maximum:190,
    required:false,
    example: 'abd alrahman shammout'
  })
  @IsString()
  @IsOptional()
  @MinLength(2)
  @MaxLength(190)
  name: string

  photoUrl: string

  @ApiProperty({ type: 'string', format: 'binary' })
  photo: any;
}