import { Injectable, UnauthorizedException } from "@nestjs/common";
import { UsersService } from "../users/users.service";
import { SignupDto } from "./dto/signup.dto";
import { LoginDto } from "./dto/login.dto";
import { JwtPayload } from "./interfaces/jwt-payload.interface";
import { JwtService } from "@nestjs/jwt";
import { AuthResponseInterface } from "./interfaces/auth.response.interface";
const bcrypt = require('bcryptjs')

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signup(signupDto: SignupDto): Promise<AuthResponseInterface>{

    const user = await this.usersService.createUser(signupDto)
    const payload: JwtPayload = {
      id: user.id,
      username :user.username
    };
    const accessToken = this.jwtService.sign(payload)
    return { user, accessToken }
  }



  async login(loginDto: LoginDto): Promise<AuthResponseInterface>{
    const { username, password } = loginDto;

    const user = await this.usersService.findByUsername(username);
    if (user && bcrypt.compare(password, user.password)){
      const payload: JwtPayload = {
        id: user.id,
        username :user.username
      };
      const accessToken = this.jwtService.sign(payload)
      delete user.password;
      return {user, accessToken}
    }else {
      throw new UnauthorizedException('Please check your login credentials');
    }
  }

}